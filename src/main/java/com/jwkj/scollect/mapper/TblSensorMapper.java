package com.jwkj.scollect.mapper;

import com.jwkj.scollect.model.TblSensor;
import com.jwkj.scollect.util.MyMapper;

public interface TblSensorMapper extends MyMapper<TblSensor> {
}