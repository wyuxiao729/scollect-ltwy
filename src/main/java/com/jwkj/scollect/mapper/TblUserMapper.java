package com.jwkj.scollect.mapper;

import com.jwkj.scollect.model.TblUser;
import com.jwkj.scollect.util.MyMapper;

public interface TblUserMapper extends MyMapper<TblUser> {
}