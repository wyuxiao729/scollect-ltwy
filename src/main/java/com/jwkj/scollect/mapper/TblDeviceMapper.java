package com.jwkj.scollect.mapper;

import com.jwkj.scollect.model.TblDevice;
import com.jwkj.scollect.util.MyMapper;

public interface TblDeviceMapper extends MyMapper<TblDevice> {
}