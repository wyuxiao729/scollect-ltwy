package com.jwkj.scollect.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwkj.scollect.conf.SystemGloble;
import com.jwkj.scollect.mapper.TblDeviceMapper;
import com.jwkj.scollect.mapper.TblSensorMapper;
import com.jwkj.scollect.model.TblDevice;
import com.jwkj.scollect.model.TblSensor;
import com.jwkj.scollect.service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService{

	@Autowired
	private TblDeviceMapper deviceMapper;
	@Autowired
	private TblSensorMapper sensorMapper;
	
	@Override
	public List<TblDevice> getDevicesByIp(String flag) {
		TblDevice device = new TblDevice();
		device.setDevIp(flag);
		List<TblDevice> devices = deviceMapper.select(device);
		return devices;
	}

	@Override
	public List<TblSensor> getSensorByDid(String deviceId) {
		TblSensor sensor = new TblSensor();
		sensor.setDeviceId(deviceId);
		List<TblSensor> sensors = sensorMapper.select(sensor);
		return sensors;
	}

	@Override
	public TblDevice getDevicesByAddress(int address) {
		TblDevice device = new TblDevice();
		device.setDevIp(SystemGloble.getParam(SystemGloble.COLLECTOR_CODE));
		device.setPort(String.valueOf(address));
		TblDevice temp = deviceMapper.selectOne(device);
		return temp;
	}

}
