package com.jwkj.scollect.service;

import java.util.List;

import com.jwkj.scollect.model.TblDevice;
import com.jwkj.scollect.model.TblSensor;

public interface DeviceService {
	
	/**
	 * 根据采集器编辑查询设备
	 * @return
	 */
	public List<TblDevice> getDevicesByIp(String flag);

	/**
	 * 根据采集器查询传感器
	 * @return
	 */
	public List<TblSensor> getSensorByDid(String deviceId);
	
	public TblDevice getDevicesByAddress(int address);
}
