package com.jwkj.scollect.collector.ltwy.codec;

/**
 * 解码接口
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月19日
 */
public interface ModbusRTUDecoder<T> {
	/**
	 * 解码命令数据
	 * @Author lixiaolong
	 * @CreateDate
	 * @param data
	 * @return
	 */
	public T decodeTx(byte[] data);
	/**
	 * 解码响应数据
	 * @Author lixiaolong
	 * @CreateDate
	 * @param data
	 * @return
	 */
	public T decodeRx(byte[] data);
}
