package com.jwkj.scollect.collector.ltwy.data._06;

import com.jwkj.scollect.collector.ltwy.data._10.MultiWriteRxDataGram;
import com.jwkj.scollect.collector.ltwy.data.base.ModbusFunction;
import com.jwkj.scollect.collector.ltwy.util.ByteUtils;

/**
 * 单笔写入命令响应结果
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月20日
 */
public class OneWriteRxDataGram extends MultiWriteRxDataGram {

	int value;

	/**
	 * 构造响应对象
	 * 
	 * @param addr
	 * @param start
	 * @param length
	 */
	public OneWriteRxDataGram(byte addr, int start, int length) {
		super(addr, ModbusFunction.ONE_WRITE, start, length);
	}

	/**
	 * @param buf
	 */
	public OneWriteRxDataGram(byte[] buf) {
		super(buf);
		this.value = ByteUtils.bytes2Int(this.datas, 2, 2);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}