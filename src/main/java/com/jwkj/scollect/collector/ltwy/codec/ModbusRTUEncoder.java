package com.jwkj.scollect.collector.ltwy.codec;

/**
 * 编码接口
 * @author lixiaolong
 * @CreateDate 2018年3月19日
 */
public interface ModbusRTUEncoder<T> {
	/**
	 * 编码数据,不分命令还是响应
	 * @Author lixiaolong
	 * @CreateDate
	 * @param data
	 * @return
	 */
	public byte[] encode(T data);
}
