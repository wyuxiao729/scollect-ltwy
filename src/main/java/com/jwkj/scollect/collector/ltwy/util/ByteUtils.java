package com.jwkj.scollect.collector.ltwy.util;

import java.util.Arrays;

/**
 *
 *
 * @Copyright Copyright © 2018 http://www.wanzhongonline.com/ Inc. All rights
 *            reserved.
 * @Company 万众科技
 * @Author <a href="mailto:huangkui@revenco.com">huangkui</a>
 * @CreateDate 2018年03月15日 下午3:24:49
 * @Since V1.0
 * @Version V1.0
 */
public class ByteUtils {

	public static final int MIN_MID_VALUE = 1;

	public static final int MAX_MID_VALUE = 65535;

	//bytes 转二进制整数
	public static int bytes2Int(byte[] b, int start, int length) {
		int sum = 0;
		int end = start + length;
		for (int k = start; k < end; k++) {
			int n = ((int) b[k]) & 0xff;
			n <<= (--length) * 8;
			sum += n;
		}
		return sum;
	}

	//int按长度转byte
	public static byte[] int2Bytes(int value, int length) {
		byte[] b = new byte[length];
		for (int k = 0; k < length; k++) {
			b[length - k - 1] = (byte) ((value >> 8 * k) & 0xff);
		}
		return b;
	}

	//判断是否传了mid
	public static boolean isValidofMid(int mId) {
		if (mId < MIN_MID_VALUE || mId > MAX_MID_VALUE) {
			return false;
		}
		return true;
	}

	/***
	 * byte转16进制字符串
	 */
	public static String parseByte2HexStr(byte[] buf) {
		if (null == buf) {
			return null;
		}

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			if (i != 0) {
				sb.append(" ");
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * byte数组转字符串
	 * 
	 * @param buf
	 * @return
	 */
	public static String byte2String(byte[] buf, int start, int end) {
		byte[] datas = Arrays.copyOfRange(buf, start, end);
		String hexStr = parseByte2HexStr(datas);
		String value = hexString2String(hexStr);
		return value;
	}

	/**
	 * Convert byte[] to hex
	 * string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。
	 *
	 * @param src
	 *            byte[] data
	 * @return hex string
	 */
	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			if (i != 0) {
				stringBuilder.append(" ");
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString().toUpperCase();
	}

	/**
	 * Convert hex string to byte[]
	 * 
	 * @param hexString
	 *            the hex string
	 * @return byte[]
	 */
	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase().replaceAll(" ", "");
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}

	/**
	 * Convert char to byte
	 * 
	 * @param c
	 *            char
	 * @return byte
	 */
	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	/**
	 * @Title:bytes2HexString @Description:字节数组转16进制字符串 @param b 字节数组 @return
	 * 16进制字符串 @throws
	 */
	public static String bytes2HexString(byte[] b) {
		StringBuffer result = new StringBuffer();
		String hex;
		for (int i = 0; i < b.length; i++) {
			hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			if (i != 0) {
				result.append(" ");
			}
			result.append(hex.toUpperCase());
		}
		return result.toString();
	}

	/**
	 * @Title:hexString2Bytes @Description:16进制字符串转字节数组 @param src 16进制字符串 @return
	 * 字节数组 @throws
	 */
	public static byte[] hexString2Bytes(String src) {
		src = src.replaceAll(" ", "");
		int l = src.length() / 2;
		byte[] ret = new byte[l];
		for (int i = 0; i < l; i++) {
			ret[i] = (byte) Integer.valueOf(src.substring(i * 2, i * 2 + 2), 16).byteValue();
		}
		return ret;
	}

	/**
	 * @Title:string2HexString @Description:字符串转16进制字符串 @param strPart 字符串 @return
	 * 16进制字符串 @throws
	 */
	public static String string2HexString(String strPart) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < strPart.length(); i++) {
			int ch = (int) strPart.charAt(i);
			String strHex = Integer.toHexString(ch);
			hexString.append(strHex);
		}
		return hexString.toString();
	}

	/**
	 * @Title:hexString2String @Description:16进制字符串转字符串 @param src 16进制字符串 @return
	 * 字节数组 @throws
	 */
	public static String hexString2String(String src) {
		src = src.replaceAll(" ", "");
		String temp = "";
		for (int i = 0; i < src.length() / 2; i++) {
			temp = temp + (char) Integer.valueOf(src.substring(i * 2, i * 2 + 2), 16).byteValue();
		}
		return temp;
	}

	/**
	 * 16进制字符串转int整整
	 * 
	 * @param src
	 * @return
	 */
	public static int hexString2Int(String src) {
		return Integer.parseInt(src, 16);
	}
	/*
	 * public static byte[] string2Bytes(String src){ String hexString =
	 * string2HexString(src); byte[] bytes = hexString2Bytes(hexString); return
	 * bytes; }
	 */

	/**
	 * String 转byte数组
	 * 
	 * @param src
	 * @return
	 */
	public static byte[] string2Bytes(String src) {
		byte[] bytes = new byte[src.length()];
		for (int i = 0; i < src.length(); i++) {
			bytes[i] = (byte) src.charAt(i);
		}
		return bytes;
	}

	/**
	 * @Title:char2Byte @Description:字符转成字节数据char-->integer-->byte @param
	 * src @return @throws
	 */
	public static Byte char2Byte(Character src) {
		return Integer.valueOf((int) src).byteValue();
	}

	/**
	 * @Title:intToHexString @Description:10进制数字转成16进制 @param a 转化数据 @param len
	 * 占用字节数 @return @throws
	 */
	public static String intToHexString(int a, int len) {
		len <<= 1;
		String hexString = Integer.toHexString(a);
		int b = len - hexString.length();
		if (b > 0) {
			for (int i = 0; i < b; i++) {
				hexString = "0" + hexString;
			}
		}
		return hexString;
	}

	public static void main(String args[]) {
		System.out.println(hexString2String("3133383131313536373838"));
	}
}
