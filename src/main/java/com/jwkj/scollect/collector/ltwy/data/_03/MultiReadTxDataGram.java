package com.jwkj.scollect.collector.ltwy.data._03;


import com.jwkj.scollect.collector.ltwy.data.base.ModbusDataGram;
import com.jwkj.scollect.collector.ltwy.data.base.ModbusFunction;
import com.jwkj.scollect.collector.ltwy.util.ByteUtils;

/**
 * 多笔读取查询命令
 * 
 * <pre>
 * Addr：从机地址
 * Fun：功能码
 * Data start reg hi：数据起始地址 寄存器高字节
 * Data start reg lo：数据起始地址 寄存器低字节
 * Data #of reg hi：数据读取个数 寄存器高字节
 * Data #of reg lo：数据读取个数 寄存器低字节
 * CRC16 Hi: 循环冗余校验 高字节
 * CRC16 Lo: 循环冗余校验 低字节
 * </pre>
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月19日
 */

public class MultiReadTxDataGram extends ModbusDataGram {

	/**
	 * 数据区长度
	 */
	private static final int DATAS_LENGTH = 4;
	int start;//寄存器起始地址
	int length;//连续读取的寄存器数量

	public MultiReadTxDataGram(byte[] buf) {
		super(buf);
		//寄存器起始地址
		this.start = ByteUtils.bytes2Int(this.datas, 0, 2);
		//数据区字节数
		this.length = ByteUtils.bytes2Int(this.datas, 2, 2);
	}

	/**
	 * 构造器,设置起始寄存器和读取数量,并初始化数据区和校验区
	 * 
	 * @param addr
	 * @param start
	 * @param length
	 */
	public MultiReadTxDataGram(int addr, int start, int length) {
		super(ByteUtils.int2Bytes(addr, 1)[0], ModbusFunction.MULTI_READ.getCode());
		this.start = start;
		this.length = length;
		this.datas = madeDatas();
		this.check = checkDataBytes();
	}

	@Override
	public byte[] madeDatas() {
		byte[] data = new byte[DATAS_LENGTH];//起始位2个字节+长度两个字节
		//起始位置
		byte[] regStart = ByteUtils.int2Bytes(start, 2);
		data[0] = regStart[0];
		data[1] = regStart[1];
		//长度(寄存器数量)
		byte[] regLength = ByteUtils.int2Bytes(length, 2);
		data[2] = regLength[0];
		data[3] = regLength[1];
		return data;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
