package com.jwkj.scollect.collector.ltwy.data._10;


import com.jwkj.scollect.collector.ltwy.data.base.ModbusDataGram;
import com.jwkj.scollect.collector.ltwy.data.base.ModbusFunction;
import com.jwkj.scollect.collector.ltwy.util.ByteUtils;

/**
 * 多笔写入命令响应结果
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月20日
 */
public class MultiWriteRxDataGram extends ModbusDataGram {

	int start;//寄存器起始地址
	int length;//写入笔数(写入寄存器个数)

	/**
	 * 用报文来构造对象
	 * 
	 * @param buf
	 */
	public MultiWriteRxDataGram(byte[] buf) {
		super(buf);
		//start
		this.start = ByteUtils.bytes2Int(this.datas, 0, 2);
		if (this.funCode == ModbusFunction.MULTI_WRITE.getCode()) {
			//length
			this.length = ByteUtils.bytes2Int(this.datas, 2, 2);
		}
		if (this.funCode == ModbusFunction.ONE_WRITE.getCode()) {
			this.length = 1;
		}
	}

	/**
	 * 构造响应对象
	 * 
	 * @param addr
	 * @param fun
	 * @param start
	 * @param length
	 */
	public MultiWriteRxDataGram(byte addr, ModbusFunction fun, int start, int length) {
		super(addr, fun.getCode());
		this.start = start;
		this.length = length;
		this.datas = madeDatas();
		this.check = checkDataBytes();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mtech.aircdx.datagram.data.ModbusDataGram#madeDatas()
	 */
	@Override
	protected byte[] madeDatas() {
		byte[] datas = new byte[2 + 2];
		byte[] starts = ByteUtils.int2Bytes(this.start, 2);
		datas[0] = starts[0];
		datas[1] = starts[1];
		byte[] lengths = ByteUtils.int2Bytes(this.length, 2);
		datas[2] = lengths[0];
		datas[3] = lengths[1];
		return datas;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}