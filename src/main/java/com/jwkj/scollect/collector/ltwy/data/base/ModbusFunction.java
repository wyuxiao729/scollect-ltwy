package com.jwkj.scollect.collector.ltwy.data.base;

/**
 * 功能码
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月19日
 */
public enum ModbusFunction {
	/**
	 * 03:多笔读取
	 */
	MULTI_READ((byte) 0x03),
	/**
	 * 06:单笔写入
	 */
	ONE_WRITE((byte) 0x06),
	/**
	 * 10:多笔写入
	 */
	MULTI_WRITE((byte) 0x10);
	byte code;

	ModbusFunction(byte code) {
		this.code = code;
	}

	public byte getCode() {
		return this.code;
	}

}
