package com.jwkj.scollect.collector.ltwy.data._03;


import com.jwkj.scollect.collector.ltwy.data.base.ModbusDataGram;
import com.jwkj.scollect.collector.ltwy.data.base.ModbusFunction;
import com.jwkj.scollect.collector.ltwy.util.ByteUtils;

/**
 * 多笔读取查询返回结果
 * 
 * <pre>
 * Addr：从机地址
 * Fun：功能码
 * Data start reg hi：数据起始地址 寄存器高字节
 * Data start reg lo：数据起始地址 寄存器低字节
 * Data #of reg hi：数据读取个数 寄存器高字节
 * Data #of reg lo：数据读取个数 寄存器低字节
 * CRC16 Hi: 循环冗余校验 高字节
 * CRC16 Lo: 循环冗余校验 低字节
 * </pre>
 * 
 * @author lixiaolong
 * @CreateDate 2018年3月19日
 */

public class MultiReadRxDataGram extends ModbusDataGram {

	int[] regDatas;//寄存器数据
	byte length;//数据区寄存器字节数

	/**
	 * 构造一个03查询返回对象
	 * 
	 * @param addr
	 *            设备地址
	 * @param regDatas
	 *            寄存器数据
	 */
	public MultiReadRxDataGram(byte addr, int[] regDatas) {
		super(addr, ModbusFunction.MULTI_READ.getCode());
		this.length = ByteUtils.int2Bytes(2 * regDatas.length, 1)[0];
		this.regDatas = regDatas;
		this.datas = madeDatas();
		this.check = checkDataBytes();
	}

	/**
	 * 用读取到的数据帧进行初始化
	 * 
	 * @param buf
	 *            读取到的数据帧
	 */
	public MultiReadRxDataGram(byte[] buf) {
		super(buf);
		//数据区字节数
		this.length = buf[2];
		
		//获取数据区数据(寄存器数据)
		this.regDatas = new int[this.length / 2];
		for (int i = 0, j = 0; i < this.length; i += 2, j++) {
			int value = ByteUtils.bytes2Int(buf, 3 + i, 2);
			regDatas[j] = value;
		}
	}

	/*
	 * 根据数据的字节长度和寄存器数据构造成数据区
	 * 
	 * @see com.mtech.aircdx.datagram.data.ModbusDataGram#madeDatas()
	 */
	@Override
	protected byte[] madeDatas() {
		byte[] data = new byte[1 + this.length];
		data[0] = this.length;
		for (int i = 0; i < regDatas.length; i++) {
			byte[] b = ByteUtils.int2Bytes(regDatas[i], 2);
			data[2 * i + 1] = b[0];
			data[2 * i + 2] = b[1];
		}
		return data;
	}

	public byte getLength() {
		return length;
	}

	public void setLength(byte length) {
		this.length = length;
	}

	public int[] getRegDatas() {
		return regDatas;
	}

	public void setRegDatas(int[] regDatas) {
		this.regDatas = regDatas;
	}

}
