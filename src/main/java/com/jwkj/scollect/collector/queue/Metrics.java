/*   
* Project: OSMP
* FileName: Metrics.java
* version: V1.0
*/
package com.jwkj.scollect.collector.queue;

import java.util.HashMap;
import java.util.Map;

public class Metrics {

	private String metric;
	private Long timestamp;
	private Float value;
	private Map<String, String> tags;

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the value
	 */
	public Float getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Float value) {
		this.value = value;
	}

	public Map<String, String> getTags() {
		return tags;
	}

	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

	public static Metrics of(String metric, float values, long timestamp, String ip, String code, String dcode,
			String deviceId) {
		Metrics metrics = new Metrics();
		metrics.setMetric(metric);
		metrics.setTimestamp(timestamp);
		metrics.setValue(values);
		Map<String, String> tag = new HashMap<>();
		tag.put("dip", ip);
		tag.put("scode", code);
		tag.put("did", deviceId);
		tag.put("dcode", dcode);
		metrics.setTags(tag);
		return metrics;
	}

}
