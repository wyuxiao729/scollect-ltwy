package com.jwkj.scollect.collector.queue;

import java.util.ArrayList;
import java.util.List;

/**
 * 传感器上报数据
 * @author yuying
 *
 */
public class MessageData {
	private String deviceId;
	private List<Metrics> metrics = new ArrayList<>();
	
	public static MessageData of(){
		MessageData message = new MessageData();
		return message;
	}
	
	public void put(Metrics e){
		metrics.add(e);
	}

	/**
	 * @return the metrics
	 */
	public List<Metrics> getMetrics() {
		return metrics;
	}

	/**
	 * @param metrics the metrics to set
	 */
	public void setMetrics(List<Metrics> metrics) {
		this.metrics = metrics;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
}
