package com.jwkj.scollect.collector.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.jwkj.scollect.service.IRedisService;

import com.jwkj.scollect.conf.SpringUtils;
import com.jwkj.scollect.conf.SystemGloble;
import com.jwkj.scollect.util.HttpClientUtil;

public class SendQueue {
	
	private static Logger logger = Logger.getLogger(SendQueue.class);
	
	private static String REDIS_COLL_KEY = "DEVICEDATA:";
	
	public static final BlockingQueue<MessageData> blockQueue = new LinkedBlockingQueue<>();
	
	public static void addMessageData(MessageData message) {
		try {
			blockQueue.put(message);
		} catch (InterruptedException e) {
			logger.error("Add MessageData is faild", e);
		}
	}
	
	public static void start(){
		while(true) {
			try {
				MessageData message = blockQueue.take();
				String mes = JSONObject.toJSONString(message.getMetrics());
				String result = HttpClientUtil.doPostJson(SystemGloble.getParam(SystemGloble.GRANFANA_URL), mes);
				logger.debug("send granfana data result: " + result +" message: " + mes);
				IRedisService redis = SpringUtils.getBean(IRedisService.class);
				redis.set(REDIS_COLL_KEY + message.getDeviceId(), mes);
			} catch (InterruptedException e) {
				logger.error("send granfana is failed", e);
				Thread.currentThread().interrupt();
			}
		}
	}
}
