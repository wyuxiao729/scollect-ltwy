package com.jwkj.scollect;

//特别注意，下面的是 tk.MapperScan

import com.jwkj.scollect.collector.CollectorServer;
import com.jwkj.scollect.collector.queue.SendQueue;
import com.jwkj.scollect.mock.MockServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import tk.mybatis.spring.annotation.MapperScan;

import com.jwkj.scollect.conf.SystemGloble;

/**
 * @author wangkaiping
 * @since 2018-2-12 18:22
 */
@Controller
@EnableWebMvc
@SpringBootApplication
@MapperScan(basePackages = "com.jwkj.scollect.mapper")
public class Application extends WebMvcConfigurerAdapter implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		new Thread() {
			@Override
			public void run() {
				SendQueue.start();
			}
		}.start();
		if(SystemGloble.getParam(SystemGloble.IS_MOCK).equals("true")) {
			logger.info("start mock server ...");
			new Thread() {
				public void run() {
					try {
						MockServer.init();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				};
			}.start();
		}
		CollectorServer server = CollectorServer.getInstance();
		server.setPort(9527);
		Thread th = new Thread(server);
		th.start();

		logger.info("服务启动完成!");
	}

}
