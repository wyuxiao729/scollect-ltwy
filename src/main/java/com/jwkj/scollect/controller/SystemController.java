/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 abel533@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.jwkj.scollect.controller;

import com.jwkj.scollect.collector.CollectorServer;
import com.jwkj.scollect.service.DeviceService;
import com.jwkj.scollect.service.IRedisService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jwkj.scollect.model.TblDevice;
import com.jwkj.scollect.model.TblSensor;

/**
 * 系统管理类
 * @author yuying
 *
 */
@RestController
@RequestMapping("/system")
public class SystemController {

	@Autowired
	private IRedisService redisService;

	@Autowired
	private DeviceService deviceService;

	/**
	 * 根据采集器编号查询当前数据
	 * @param key
	 * @return
	 */
	@RequestMapping("/getCurrentData/data/{key}")
	@ResponseBody
	public Object getCurrentData(@PathVariable("key") String key) {
		String value = redisService.get(key);
		return value;
	}

	/**
	 * 清除设备缓存
	 * @return
	 */
	@RequestMapping("/cleanDeviceMapping")
	@ResponseBody
	public Object cleanDeviceMapping() {
		CollectorServer.cleanDeviceMapping();
		return true;
	}

	/**
	 * 获取采集器和设备映射
	 * @return
	 */
	@RequestMapping("/getDeviceMapping")
	@ResponseBody
	public Object getByResi() {
		return CollectorServer.getDeviceMapping();
	}

	@RequestMapping("/getDevice")
	@ResponseBody
	public Object getDevice() {
		TblDevice devices = deviceService.getDevicesByAddress(8);
		List<TblSensor> sensors = deviceService.getSensorByDid(devices.getId());
		return sensors;
	}

}
