package com.jwkj.scollect.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ParamConfig {

	@Autowired
	private Environment env;

	@Bean
	public int readConf() {
		SystemGloble.setParam(SystemGloble.GRANFANA_URL, env.getProperty("granfana.url"));
		SystemGloble.setParam(SystemGloble.COLLECTOR_CODE, env.getProperty("collector.code"));
		SystemGloble.setParam(SystemGloble.MOCK_FLAG, env.getProperty("collector.mock.flag"));
		SystemGloble.setParam(SystemGloble.IS_MOCK, env.getProperty("mock"));
		return 1;
	}

}
