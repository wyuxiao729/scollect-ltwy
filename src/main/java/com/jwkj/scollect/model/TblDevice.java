package com.jwkj.scollect.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tbl_device")
public class TblDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "create_date_time")
    private Date createDateTime;

    private Integer deleted;

    @Column(name = "update_date_time")
    private Date updateDateTime;

    private Integer version;

    @Column(name = "org_id")
    private String orgId;

    private String type;

    private String px;

    private String py;

    @Column(name = "level_code")
    private String levelCode;

    @Column(name = "dev_ip")
    private String devIp;

    private String port;

    private String remark;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return create_date_time
     */
    public Date getCreateDateTime() {
        return createDateTime;
    }

    /**
     * @param createDateTime
     */
    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * @return deleted
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * @param deleted
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * @return update_date_time
     */
    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * @param updateDateTime
     */
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    /**
     * @return version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return org_id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return px
     */
    public String getPx() {
        return px;
    }

    /**
     * @param px
     */
    public void setPx(String px) {
        this.px = px;
    }

    /**
     * @return py
     */
    public String getPy() {
        return py;
    }

    /**
     * @param py
     */
    public void setPy(String py) {
        this.py = py;
    }

    /**
     * @return level_code
     */
    public String getLevelCode() {
        return levelCode;
    }

    /**
     * @param levelCode
     */
    public void setLevelCode(String levelCode) {
        this.levelCode = levelCode;
    }

    /**
     * @return dev_ip
     */
    public String getDevIp() {
        return devIp;
    }

    /**
     * @param devIp
     */
    public void setDevIp(String devIp) {
        this.devIp = devIp;
    }

    /**
     * @return port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}