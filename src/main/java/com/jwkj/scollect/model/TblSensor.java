package com.jwkj.scollect.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tbl_sensor")
public class TblSensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "create_date_time")
    private Date createDateTime;

    private Integer deleted;

    @Column(name = "update_date_time")
    private Date updateDateTime;

    private Integer version;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "io_port")
    private Integer ioPort;

    private Integer livalue;

    private String remark;

    private String unit;

    private String icon;

    private byte[] type;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return create_date_time
     */
    public Date getCreateDateTime() {
        return createDateTime;
    }

    /**
     * @param createDateTime
     */
    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * @return deleted
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * @param deleted
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * @return update_date_time
     */
    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * @param updateDateTime
     */
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    /**
     * @return version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return device_id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return io_port
     */
    public Integer getIoPort() {
        return ioPort;
    }

    /**
     * @param ioPort
     */
    public void setIoPort(Integer ioPort) {
        this.ioPort = ioPort;
    }

    /**
     * @return livalue
     */
    public Integer getLivalue() {
        return livalue;
    }

    /**
     * @param livalue
     */
    public void setLivalue(Integer livalue) {
        this.livalue = livalue;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return type
     */
    public byte[] getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(byte[] type) {
        this.type = type;
    }
}